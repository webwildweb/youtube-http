var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable, } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchResult } from './search-result.model';
import { map } from 'rxjs/operators';
/**
 * YouTubeService connects to the YouTube API
 * See: * https://developers.google.com/youtube/v3/docs/search/list
 */
var YouTubeSearchService = /** @class */ (function () {
    function YouTubeSearchService(http) {
        this.http = http;
        this.YOUTUBE_API_KEY = 'AIzaSyBctUUwEc9DoHwEW4AA8vx35xZxjhx5VCo';
        this.YOUTUBE_API_URL = 'https://www.googleapis.com/youtube/v3/search';
    }
    YouTubeSearchService.prototype.search = function (query) {
        var params = [
            "q=" + query,
            "key=" + this.YOUTUBE_API_KEY,
            "part=snippet",
            "type=video",
            "maxResults=10"
        ].join('&');
        var queryUrl = this.YOUTUBE_API_URL + "?" + params;
        return this.http.get(queryUrl).pipe(map(function (response) {
            return response.items.map(function (item) {
                // console.log("raw item", item); // uncomment if you want to debug
                return new SearchResult({
                    id: item.id.videoId,
                    title: item.snippet.title,
                    description: item.snippet.description,
                    thumbnailUrl: item.snippet.thumbnails.high.url
                });
            });
        }));
    };
    YouTubeSearchService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], YouTubeSearchService);
    return YouTubeSearchService;
}());
export { YouTubeSearchService };
//# sourceMappingURL=you-tube-search.service.js.map