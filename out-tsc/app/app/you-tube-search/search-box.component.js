var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Output, EventEmitter, ElementRef } from '@angular/core';
// By importing just the rxjs operators we need, We're theoretically able
// to reduce our build size vs. importing all of them.
import { fromEvent } from 'rxjs';
import { YouTubeSearchService } from './you-tube-search.service';
import { debounceTime, filter, map, switchAll, tap } from 'rxjs/operators';
var SearchBoxComponent = /** @class */ (function () {
    function SearchBoxComponent(youtube, el) {
        this.youtube = youtube;
        this.el = el;
        this.loading = new EventEmitter();
        this.results = new EventEmitter();
    }
    SearchBoxComponent.prototype.ngOnInit = function () {
        var _this = this;
        // convert the `keyup` event into an observable stream
        console.log('ele', this.el.nativeElement);
        var obs = fromEvent(this.el.nativeElement, 'keyup')
            .pipe(map(function (e) { return e.target.value; }), // extract the value of the input
        filter(function (text) { return text.length > 1; }), // filter out if empty
        debounceTime(250), // only search after 250 ms
        tap(function () { return _this.loading.emit(true); }), // Enable loading
        // search, call the search service
        map(function (query) { return _this.youtube.search(query); }), 
        // discard old events if new input comes in
        switchAll()
        // act on the return of the search
        ).subscribe(function (results) {
            _this.loading.next(false);
            _this.results.next(results);
        }, function (err) {
            console.log(err);
            _this.loading.next(false);
        }, function () {
            _this.loading.next(false);
        });
        // Update version rxjs to new version
        /*Observable.fromEvent(this.el.nativeElement, 'keyup')
          .map((e: any) => e.target.value) // extract the value of the input
          .filter((text: string) => text.length > 1) // filter out if empty
          .debounceTime(250)                         // only once every 250ms
          .do(() => this.loading.next(true))         // enable loading
          // search, discarding old events if new input comes in
          .map((query: string) => this.youtube.search(query))
          .switch()
          // act on the return of the search
          .subscribe(
            (results: SearchResult[]) => { // on sucesss
              this.loading.next(false);
              this.results.next(results);
            },
            (err: any) => { // on error
              console.log(err);
              this.loading.next(false);
            },
            () => { // on completion
              this.loading.next(false);
            }
          );*/
    };
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], SearchBoxComponent.prototype, "loading", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], SearchBoxComponent.prototype, "results", void 0);
    SearchBoxComponent = __decorate([
        Component({
            selector: 'app-search-box',
            template: "\n    <input type=\"text\" class=\"form-control\" placeholder=\"Search\" autofocus>\n  "
        }),
        __metadata("design:paramtypes", [YouTubeSearchService,
            ElementRef])
    ], SearchBoxComponent);
    return SearchBoxComponent;
}());
export { SearchBoxComponent };
//# sourceMappingURL=search-box.component.js.map