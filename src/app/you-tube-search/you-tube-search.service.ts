import {
  Injectable,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SearchResult } from './search-result.model';
import { map } from 'rxjs/operators';

/**
 * YouTubeService connects to the YouTube API
 * See: * https://developers.google.com/youtube/v3/docs/search/list
 */
@Injectable()
export class YouTubeSearchService {

  YOUTUBE_API_KEY = 'AIzaSyBctUUwEc9DoHwEW4AA8vx35xZxjhx5VCo';
  YOUTUBE_API_URL = 'https://www.googleapis.com/youtube/v3/search';

  constructor(private http: HttpClient) {
    }

    search(query: string): Observable<SearchResult[]> {
      const params: string = [
        `q=${query}`,
        `key=${this.YOUTUBE_API_KEY}`,
        `part=snippet`,
        `type=video`,
        `maxResults=10`
      ].join('&');
      const queryUrl = `${this.YOUTUBE_API_URL}?${params}`;
      return this.http.get(queryUrl).pipe(
        map((response: any) => {
          return response.items.map(item => {
            // console.log("raw item", item); // uncomment if you want to debug
            return new SearchResult({
              id: item.id.videoId,
              title: item.snippet.title,
              description: item.snippet.description,
              thumbnailUrl: item.snippet.thumbnails.high.url
            });
          });
        })
      );
    }
  }
